#include <stdio.h>

struct personas{
    char nombre[20];
    int edad;
};

int main(){
    char mas_joven[20], mas_viejo[20];

    struct personas per1[5];

    for (int i = 0; i < 5; i++)
    {
        printf("Ingrese el Nombre de la Persona: ");
        scanf("%s", per1[i].nombre);
        printf("\nIngrese la edad: ");
        scanf("%d", &per1[i].edad);

    }

    for (int i = 0; i < 5; i++)
    {
        //Mas viejo
        if(per1[i].edad > per1[i+1].edad) mas_viejo = per1[i].nombre;

        //Mas joven
        if (per1[i].edad < per1[i+1].edad) mas_joven = per1[i].nombre;
    }
    
    printf("El mas viejo es %s \n",mas_viejo);
    printf("El mas joven es %s \n",mas_joven);
    
    return 0;

}